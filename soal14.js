const volBalok = (...args)=>{
    let vol = args[0] * args[1] * args[2]
    return console.log(`Volume balok dengan panjang=${args[0]}, lebar=${args[1]}, dan tinggi=${args[2]} adalah ${vol}`)
}
  
const volKubus = (...args) =>{
    let vol = args[0]*args[0]*args[0]
    return console.log(`Volume kubus dengan panjang sisi=${args[0]} adalah ${vol}`)
}

volBalok(4,5,6,7)

volKubus(2,3)
  