import React from 'react';
import './soal13.css';

const dataP = [
    {name: "John", age: 25, gender: "Male", profession: "Engineer", photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"},
    {name: "Sarah", age: 22, gender: "Female", profession: "Designer", photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"},
    {name: "David", age: 30, gender: "Male", profession: "Programmer", photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"},
    {name: "Kate", age: 27, gender: "Female", profession: "Model", photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" }
]

function gelar(jk) {
    let panggilan="";
    if (jk=="Male") {
        panggilan="Mr";
    } else if (jk=="Female") {
        panggilan="Mrs";
    } else {
        panggilan="undefined"
    }
    return panggilan;
}

class Dataa extends React.Component {
    render() {
      return (
        <div class="kotak">
            <div class="batas">
                <div class="gambar">
                    <img src={this.props.foto}/>
                </div>
                <div class="details">
                    <div class="nama detail">{gelar(this.props.jk)} {this.props.nama}</div>
                    <div class="detail">{this.props.profesi}</div>
                    <div class="detail">{this.props.usia} years old</div>
                </div>
            </div>
        </div>
      );
    }
}

export default class Tugas10 extends React.Component {

    render() {
        return (
            <div className="App">
                <div class="row">
                {dataP.map(el => {
                    return (
                        <Dataa jk={el.gender} foto={el.photo} nama={el.name} profesi={el.profession} usia={el.age}/>
                    )
                })}
                </div>
            </div>
        );
    }
}